#Just Text

This is a simple theme for text based websites, based on „Hugo Simple Theme“, a very simple and lightweight Hugo Theme inspired by the geniuses behind bettermotherfuckingwebsite.com.

## Screenshots

![preview](https://raw.githubusercontent.com/Xzya/simple-hugo-theme/master/images/screenshot.png)
![preview](https://raw.githubusercontent.com/Xzya/simple-hugo-theme/master/images/screenshot2.png)

## Configuration

Check `exampleSite/config.toml` for an example configuration.

## Menu

The navbar displays the `main` menus by default. You can find more details about how to configure it [here](https://gohugo.io/templates/menu-templates/), as well as in the `exampleSite`.

## License

Open sourced under the [MIT license](./LICENSE.md).